const fs = require("fs");
const Tx = require("ethereumjs-tx").Transaction;
const Web3 = require("web3");
const Common = require("ethereumjs-common").default;
const Config = JSON.parse(fs.readFileSync("config.json", "utf-8"));

const web3 = new Web3(
    new Web3.providers.HttpProvider(
        "https://speedy-nodes-nyc.moralis.io/0870352740b8d3e50b81a654/bsc/mainnet"
    )
);
const BSC_FORK = Common.forCustomChain(
    "mainnet",
    {
        name: "Binance Smart Chain Mainnet",
        networkId: 56,
        chainId: 56,
        url: "https://speedy-nodes-nyc.moralis.io/0870352740b8d3e50b81a654/bsc/mainnet",
    },
    "istanbul"
);

// SPECIFY_THE_AMOUNT_OF_BNB_YOU_WANT_TO_BUY_FOR_HERE
var originalAmountToBuyWith = Config.buyAmmount; // BNB
var bnbAmount = web3.utils.toWei(originalAmountToBuyWith, "ether");

var targetAccount = Config.account;
console.log(
    `Buying ONLYONE for ${originalAmountToBuyWith} BNB from pancakeswap for address ${targetAccount.address}`
);

var res = buyOnlyone(targetAccount, bnbAmount);
console.log(res);

async function buyOnlyone(targetAccount, amount) {
    var amountToBuyWith = web3.utils.toHex(amount);
    var privateKey = Buffer.from(targetAccount.privateKey, "hex");
    var pancakeSwapRouterAddress = Config.pancakeSwapRouterAddress;
    var slippage = Config.slippage; // Slippage (%)

    const pairAddress = Config.pair;
    var routerAbi = Config.pancakeRouterAbi;

    // Create contract with pancake router
    let contract = await new web3.eth.Contract(
        routerAbi,
        pancakeSwapRouterAddress
    );
    let amounts = null;
    let i = 1;
    while (true) {
        try {
            // Get ammount
            amounts = await contract.methods
                .getAmountsOut(amountToBuyWith, pairAddress)
                .call();
            if (amounts) {
                break;
            }
        } catch (e) {
            console.log(i + " Checking...");
        }
        await sleep(1000);
        i++;
    }

    // Check ammount out
    if (!checkXnxx(amounts)) {
        console.log('Out cmnr');
        return;
    }

    let amountOutMin = amounts[1] - (amounts[1] * slippage) / 100;
    amountOutMin = Number(amountOutMin).toLocaleString("fullwide", {
        useGrouping: false,
    });
    if (pairAddress[0] == Config.bnbAddress) {
        // bnb to token
        var data = contract.methods.swapExactETHForTokens(
            web3.utils.toHex(amountOutMin),
            pairAddress,
            targetAccount.address,
            web3.utils.toHex(Math.round(Date.now() / 1000) + 60 * 20) // Max processing time (20 minutes)
        );
    } else {
        // Token to token
        var data = contract.methods.swapExactTokensForTokensSupportingFeeOnTransferTokens(
            web3.utils.toHex(amountToBuyWith), // Ammount in
            web3.utils.toHex(amountOutMin), // Ammount out min
            pairAddress,
            targetAccount.address,
            web3.utils.toHex(Math.round(Date.now() / 1000) + 60 * 20) // Max processing time (20 minutes)
        );
    }

    var count = await web3.eth.getTransactionCount(targetAccount.address);
    var rawTransaction = {
        from: targetAccount.address,
        gasPrice: web3.utils.toHex(convertGasPrice(Config.gasPrice)),
        gasLimit: web3.utils.toHex(400000), // Gas limit (100000 = 0.0005 BNB, 500000 = 0.0025)
        to: pancakeSwapRouterAddress,
        // value: web3.utils.toHex(amountToBuyWith),
        data: data.encodeABI(),
        nonce: web3.utils.toHex(count),
    };
    if (pairAddress[0] == Config.bnbAddress) {
        rawTransaction.value = web3.utils.toHex(amountToBuyWith);
    }

    var transaction = new Tx(rawTransaction, { common: BSC_FORK });
    transaction.sign(privateKey);

    var result = await web3.eth.sendSignedTransaction(
        "0x" + transaction.serialize().toString("hex")
    );
    console.log(result);
    return result;
}

function convertGasPrice(gasGwei) {
    return gasGwei * 1000000000;
}

async function getTransactionInfo(txHash) {
    var transactionInfo = await web3.eth.getTransaction(txHash);
    return transactionInfo;
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

function checkXnxx(amounts) {
    let idoPrice = Config.idoPrice;
    let xnxx = Config.xnxx;
    let xnxxOutMin = (amounts[1] * (10e-19));
    let currentPrice = Config.buyAmmount / xnxxOutMin;

    // Check ammout out
    if (xnxx != '' && (currentPrice / idoPrice) > xnxx) {
        return false;
    }
    return true;
}